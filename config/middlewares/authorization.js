
/*
 *  Generic require login routing middleware
 */

exports.requiresLogin = function (req, res, next) {
  if (!req.isAuthenticated()) {
    req.session.returnTo = req.originalUrl
    req.flash('error', 'You are not authorized')
    return res.redirect('/')
  }
  next()
}

/*
 *  User authorization routing middleware
 */

exports.user = {
    hasAuthorization : function (req, res, next) {
        if (req.profile.id != req.user.id) {
            req.flash('info', 'You are not authorized')
            return res.redirect('/users/'+req.profile.id)
        }
        next()
    }
}

exports.user = {
    hasAdmin : function (req, res, next) {
        if (!req.isAuthenticated()) {
            req.session.returnTo = req.originalUrl
            req.flash('error', 'You are not authorized')
            return res.redirect('/')
        }

        if (req.user._id != 0) {
            req.flash('info', 'You are not admin')
            return res.redirect('/')
        }
        next()
    }
}

/*
 *  Article authorization routing middleware
 */

exports.article = {
  hasAuthorization : function (req, res, next) {
    if (req.article.user.id != req.user.id) {
      req.flash('info', 'You are not authorized')
      return res.redirect('/home/'+req.article.id)
    }
    next()
  }
}
