/*!
 * Module dependencies.
 */

var async = require('async')

/**
 * Controllers
 */

var users = require('../app/controllers/users')
  , home = require('../app/controllers/home')
  , chat = require('../app/controllers/chat')
  , auth = require('./middlewares/authorization')

/**
 * Route middlewares
 */

var articleAuth = [auth.requiresLogin, auth.article.hasAuthorization]

/**
 * Expose routes
 */

module.exports = function (app, passport) {

  // user routes
  app.get('/login', users.login)
  app.get('/signup', users.signup)
  app.get('/logout', users.logout)
  app.post('/users', users.create)
  app.get('/export-users', auth.user.hasAdmin, users.exportUsers)
  app.post('/users/session',
    passport.authenticate('local', {
      failureRedirect: '/',
      failureFlash: 'Invalid email/password or you are banned.'
    }), users.session)
  app.get('/users/:userId', users.show)

  app.param('userId', users.user)

  // home route
  app.get('/', home.index)

  // chat routes
  app.get('/chat', auth.requiresLogin, chat.index)

}
