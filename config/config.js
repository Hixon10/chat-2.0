
var path = require('path')
  , rootPath = path.normalize(__dirname + '/..')

module.exports = {
  development: {
    db: 'mongodb://localhost/chat',
    root: rootPath,
    app: {
      name: 'Simply Chat'
    }
  },
  test: {
    db: 'mongodb://localhost/chat',
    root: rootPath,
    app: {
      name: 'Simply Chat'
    }
  },
  production: {
      db: 'mongodb://nodejitsu_denisdenis20000:mtg6730dsq62p6948b4cf0a6sd@ds047008.mongolab.com:47008/nodejitsu_denisdenis20000_nodejitsudb9406999407',
      root: rootPath,
      app: {
          name: 'Simply Chat'
      }
  }
}
