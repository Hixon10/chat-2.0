$( document ).ready(function() {
    var heightHeader = 105;
    var heightFooter = 34;
    var realFullHeight = $( window ).height();
    var chatHeight = realFullHeight - heightFooter - heightHeader;

    $("#messages").height(chatHeight);
    $("#nicks-div").height(chatHeight);

    $('#joinARoomModal').on('show.bs.modal', function () {
        $('input#input-channel').val("");
    })

    $('#joinARoomModal').on('shown.bs.modal', function () {
        $('input#input-channel').focus();
    })

    $('#joinARoomModal').on('hidden.bs.modal', function () {
        $('input#input-message').focus();
    })
});