(function($){

    // create global app parameters...
    var ROOM_MAX_LENGTH = 10
    socket = null,
        clientId = null,
        nickname = $("a.navbar-link").text(),
        systemRoom = 'System',

        // holds the current room we are in
        currentRoom = systemRoom,

        // server information
        serverAddress = '',
        serverDisplayName = 'Server',

    connect();

    // bind DOM elements like button clicks and keydown
    function bindDOMEvents(){
        $('form#form-chat').on('keydown', function(e){
            var key = e.which || e.keyCode;
            if(key == 13) {
                e.preventDefault();
                handleMessage();
            }
        });

        $('#joinARoomModal').on('keydown', function(e){
            var key = e.which || e.keyCode;
            if(key == 13) {
                $('button#join-a-room-button').trigger('click');
            }
        });

        $('button#join-a-room-button').on('click', function(){
            createRoom();
        });

        $("ul#ul-channels").contextmenu({
            delegate: "a.link-channel",
            menu: [
                {title: "Register", cmd: "register"},
                {title: "Close", cmd: "close"}
            ],
            select: function(event, ui) {
                if (ui.cmd == "close") {
                    closeRoom(ui.target.text());
                } else if (ui.cmd == "register") {
                    registerRoom(ui.target.text());
                }
            }
        });

        $("ul#nicks").contextmenu({
            delegate: "a.nick-link",
            menu: [
                {title: "Ban", cmd: "tryban"}
            ],
            select: function(event, ui) {
                if (ui.cmd == "tryban") {
                    tryBanUser(ui.target.text(), nickname ,currentRoom);
                }
            }
        });

        $('.nav-tabs').bind('click', function (e){
             var height = $("#messages")[0].scrollHeight;
             $("#messages").scrollTop(height);

            $('input#input-message').focus();
         });

        $(document).on( 'shown.bs.tab', 'a[data-toggle="tab"]', function (e) {
            var re = /#(.+)/;
            var found = e.target.toString().match(re);
            currentRoom = found[1];

            socket.emit('getusersonchannel', { room: currentRoom });

            var height = $("#messages")[0].scrollHeight;
            $("#messages").scrollTop(height);

            $('input#input-message').focus();
        })

        $(document).on( 'dblclick', 'a.nick-link', function (e) {
            var re = /#(.+)/;
            var found = e.target.toString().match(re);
            var nick = found[1];

            var currentNick = $("p.pull-right a.navbar-link").text();

            var nicks = [nick, currentNick];
            var sortNicks = nicks.sort();

            var privateRoomName = "private-" + sortNicks[0] + "-" + sortNicks[1];

            currentRoom = privateRoomName;

            if($('ul#ul-channels li[data-roomId="' + privateRoomName + '"]').length != 0){
                $('ul#ul-channels li[data-roomId="' + privateRoomName + '"] a').trigger('click');
            } else {
                socket.emit('subscribeprivate', { room: privateRoomName, currentNick: currentNick, secondNick: nick });
            }
        })
    }

    // bind socket.io event handlers
    // this events fired in the server
    function bindSocketEvents(){

        // when the connection is made, the server emiting
        // the 'connect' event
        socket.on('connect', function(){
            // firing back the connect event to the server
            // and sending the nickname for the connected client
            socket.emit('connect', { nickname: nickname });
        });

        // after the server created a client for us, the ready event
        // is fired in the server with our clientId, now we can start
        socket.on('ready', function(data){
            // saving the clientId localy
            clientId = data.clientId;
        });

        // after the initialize, the server sends a list of
        // all the active rooms
        socket.on('roomslist', function(data){
            insertCurrentRoomList(data, systemRoom);
        });

        // when someone sends a message, the sever push it to
        // our client through this event with a relevant data
        socket.on('chatmessage', function(data){
            var nickname = data.client.nickname;
            var message = data.message;
            var room = data.room;

            //display the message in the chat window
            insertMessage(nickname, message, room);
        });

        // when we subscribes to a room, the server sends a list
        // with the clients in this room
        socket.on('roomclients', function(data){

            // add the room name to the rooms list
            addRoom(data.room, false);

            // set the current room
            setCurrentRoom(data.room);

            // announce a welcome message
            insertMessage(serverDisplayName, 'Welcome to the room: `' + data.room + '`... enjoy!', data.room);
            $('ul#nicks').empty();

            // add the clients to the clients list
            addClient({ nickname: nickname, clientId: clientId, room: data.room }, false, true);
            for(var i = 0, len = data.clients.length; i < len; i++){
                if(data.clients[i]){
                    data.clients[i].room = data.room;
                    addClient(data.clients[i], false);
                }
            }
        });

        //Обновление списка ников для некоторого канала
        socket.on('usersonchannel', function(data){
            $('ul#nicks').empty();

            // add the clients to the clients list
            addClient({ nickname: nickname, clientId: clientId, room: data.room }, false, true);
            for(var i = 0, len = data.clients.length; i < len; i++){
                if(data.clients[i]){
                    data.clients[i].room = data.room;
                    addClient(data.clients[i], false);
                }
            }
        });

        // if someone creates a room the server updates us
        // about it
        socket.on('addroom', function(data){
            socket.emit('getchannellist', { });
        });

        // if one of the room is empty from clients, the server,
        // destroys it and updates us
        socket.on('removeroom', function(data){
            if (data.room.indexOf("private-") == 0) {
                return;
            }

            socket.emit('getchannellist', { });
        });

        // with this event the server tells us when a client
        // is connected or disconnected to the current room
        socket.on('presence', function(data){
            if(data.state == 'online'){
                data.client.room = data.room;
                addClient(data.client, true);
                socket.emit('getusersonchannel', { room: currentRoom });
            } else if(data.state == 'offline'){
                data.client.room = data.room;
                removeClient(data.client, true);
            }
        });

        // open new private room
        socket.on('privateroomopen', function(data){
            if (this.socket.sessionid == data.clientId) {
                var currentNick = $("p.pull-right a.navbar-link").text();

                var nicks = [data.firstNick, currentNick];
                var sortNicks = nicks.sort();

                var privateRoomName = "private-" + sortNicks[0] + "-" + sortNicks[1];

                socket.emit('subscribeprivate', { room: privateRoomName});
            }
        });

        // respond to request to register a room
        socket.on('respondregisterroom', function(data){
            if (data.hasError) {
                insertMessage(serverDisplayName, data.error, currentRoom);
            } else {
                insertMessage(serverDisplayName, "You have successfully registered room " + data.room + "!", currentRoom);
            }
        });

        //Not permissions for ban
        socket.on('notbanuser', function(data){
            insertMessage(serverDisplayName, data.message, data.room);
        });

        //Ban user
        socket.on('banuser', function(data){
            console.log("nickname: " + nickname);
            console.log("data.user: " + data.user);
            console.log("equal:" + (nickname == data.user).toString());
            if (nickname == data.user) {
                alert("You was banned by " + data.moderator + "!");
                window.location.replace("/logout");
            }
            insertMessage(data.moderator, data.message, data.room);
        });
    }

    function insertCurrentRoomList(data, currentRoom) {
        insertMessage(serverDisplayName, 'Current Room list:', currentRoom);

        for(var i = 0, len = data.rooms.length; i < len; i++){
            // in socket.io, their is always one default room
            // without a name (empty string), every socket is automaticaly
            // joined to this room, however, we don't want this room to be
            // displayed in the rooms list
            if(data.rooms[i] != ''){
                insertMessage(serverDisplayName, "<b>" + data.rooms[i].replace('/','') + "</b>", currentRoom);
            }
        }
    }

    function getCurrentRoom() {
        return currentRoom;
    }

    //register room
    function registerRoom(room) {
        if (room != systemRoom && room.indexOf("private-") != 0) {
            socket.emit('registerroom', { room: room, user: nickname });
        } else {
            alert("You do not have permission to register this room!")
        }
    }

    //try ban user
    function tryBanUser(user, moderator, room) {
        if (user == moderator) alert("You may not ban yourself!");

        if (room.indexOf("private-") == 0) alert("You may not ban user in private room!");

        socket.emit('trybanuser', { user: user, moderator: moderator, room: room });
    }

    // close room
    function closeRoom(room) {
        if (room != systemRoom) {
            socket.emit('unsubscribe', { room: room });
            $('#' + room).remove();
            $('ul#ul-channels li[data-roomId="' + room + '"]').remove();

            if ( $('ul#ul-channels li.channel').length > 0 ) {
                if ( $('ul#ul-channels li.active').length == 0 ) {
                    $('ul#ul-channels li.channel:first a').trigger('click');
                }
            }
        } else {
            alert("You do not have permission to close the system room!")
        }
    }

    // add a room to the rooms list, socket.io may add
    // a trailing '/' to the name so we are clearing it
    function addRoom(name, announce){
        // clear the trailing '/'
        name = name.replace('/','');

        // check if the room is not already in the list
        if($('ul#ul-channels li[data-roomId="' + name + '"]').length == 0){
            var html = '<li data-roomId="' + name + '" class="channel"><a data-toggle="tab" href="#' + name + '" class="link-channel">' + name + '</a></li>';
            var $html = $($.parseHTML(html));
            $html.appendTo('ul#ul-channels')

            html = '<div id="' + name + '" class="tab-pane active"><div class="message"></div></div>';
            $html = $($.parseHTML(html));
            $html.appendTo('#messages');
            $('ul#ul-channels li[data-roomId="' + name + '"] a').trigger('click');

            // if announce is true, show a message about this room
            if(announce){
                insertMessage(serverDisplayName, 'The room `' + name + '` created...', name);
            }
        }
    }

    // remove a room from the rooms list
    function removeRoom(name, announce){
        $('ul#ul-channels li[data-roomId="' + name + '"]').remove();
        // if announce is true, show a message about this room
        if(announce){
            insertMessage(serverDisplayName, 'The room `' + name + '` destroyed...', systemRoom);
        }
    }

    // add a client to the clients list
    function addClient(client, announce, isMe){
        var html = '<li data-clientId="' + client.clientId + '" class="nick"><a href="/chat#' + client.nickname + '" class="nick-link">' + client.nickname + '</a></li>';
        var $html = $($.parseHTML(html));

        // if this is our client, mark him with color
        if(isMe){
            $html.addClass('me');
        }

        // if announce is true, show a message about this client
        if(announce){
            insertMessage(serverDisplayName, client.nickname + ' has joined the room...', client.room);
        }
        $html.appendTo('ul#nicks')
    }

    // remove a client from the clients list
    function removeClient(client, announce){
        // if announce is true, show a message about this room
        if(announce){
            insertMessage(serverDisplayName, client.nickname + ' has left the room...', client.room);
        }

        if (currentRoom == client.room) {
            socket.emit('getusersonchannel', { room: currentRoom });
        }
    }

    // every client can create a new room, when creating one, the client
    // is unsubscribed from the current room and then subscribed to the
    // room he just created, if he trying to create a room with the same
    // name like another room, then the server will subscribe the user
    // to the existing room
    function createRoom(){
        var room = $('input#input-channel').val().trim();

        if (room.indexOf("private-") == 0) return;

        var regex = /^[a-zA-Z]{2,30}$/;
        currentRoom = getCurrentRoom();

        if(room && room.length <= ROOM_MAX_LENGTH && room != currentRoom && regex.test(room)){
            // create and subscribe to the new room
            currentRoom = room;
            socket.emit('subscribe', { room: room });
        }
    }

    // sets the current room when the client
    // makes a subscription
    function setCurrentRoom(room){
        currentRoom = room;
        $('ul#ul-channels li.selected').removeClass('selected');
        $('ul#ul-channels li[data-roomId="' + room + '"]').addClass('selected');
    }

    // handle the client messages
    function handleMessage(){
        var message = $('#input-message').val().trim();
        if(message){
            if (message == "/list") {
                socket.emit('getchannellist', { });
            } else {
                currentRoom = getCurrentRoom();
                // send the message to the server with the room name
                socket.emit('chatmessage', { message: message, room: currentRoom });

                // display the message in the chat window
                insertMessage(nickname, message, currentRoom);
            }
            $('#input-message').val('');
        }
    }

    // insert a message to the chat window, this function can be
    // called with some flags
    function insertMessage(sender, message, room){

        var html = '<div class="message"><span class="time-message">[' + getTime() + ']</span>&lt;<span class="nick-message">' + sender + '</span>&gt; <span class="text-message">' + message + '</span></div>';
        var $html = $($.parseHTML(html));

        // if isMe is true, mark this message so we can
        // know that this is our message in the chat window
        if(sender === nickname){
            $html.addClass('own-message');
        } else if(sender === serverDisplayName){
            $html.addClass('server');
        }

        var selectorRoom = '#' + room;
        $html.appendTo(selectorRoom);

        var height = $("#messages")[0].scrollHeight;
        $("#messages").scrollTop(height);
    }

    // return a short time format for the messages
    function getTime(){
        var date = new Date();
        return (date.getHours() < 10 ? '0' + date.getHours().toString() : date.getHours()) + ':' +
            (date.getMinutes() < 10 ? '0' + date.getMinutes().toString() : date.getMinutes());
    }

    // after selecting a nickname we call this function
    // in order to init the connection with the server
    function connect(){
        // show connecting message
        $('#messages').html('Connecting...');

        // creating the connection and saving the socket
        socket = io.connect(serverAddress);

        // now that we have the socket we can bind events to it
        bindSocketEvents();
    }

    // on document ready, bind the DOM elements to events
    $(function(){
        bindDOMEvents();
    });

})(jQuery);