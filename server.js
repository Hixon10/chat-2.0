/**
 * Module dependencies.
 */

var express = require('express')
  , fs = require('fs')
  , passport = require('passport')

/**
 * Main application entry file.
 * Please note that the order of loading is important.
 */

// Load configurations
// if test env, load example file
var env = process.env.NODE_ENV || 'development'
  , config = require('./config/config')[env]
  , mongoose = require('mongoose')
  , socketio = require('socket.io')

// Bootstrap db connection
mongoose.connect(config.db)

// Bootstrap models
var models_path = __dirname + '/app/models'
fs.readdirSync(models_path).forEach(function (file) {
  if (~file.indexOf('.js')) require(models_path + '/' + file)
})

// bootstrap passport config
require('./config/passport')(passport, config)

var app = express()
// express settings
require('./config/express')(app, config, passport)

// Bootstrap routes
require('./config/routes')(app, passport)

// Start the app by listening on <port>
var port = process.env.PORT || 3000
var server = app.listen(port)
var io = socketio.listen(server);
console.log('Express app started on port '+port)

// expose app
exports = module.exports = app


// hash object to save clients data,
// { socketid: { clientid, nickname }, socketid: { ... } }
chatClients = new Object();

var systemRoom = 'System';

// socket.io events, each connection goes through here
// and each event is emited in the client.
// I created a function to handle each event
io.sockets.on('connection', function(socket){

    // after connection, the client sends us the
    // nickname through the connect event
    socket.on('connect', function(data){
        connect(socket, data);
    });

    // when a client sends a messgae, he emits
    // this event, then the server forwards the
    // message to other clients in the same room
    socket.on('chatmessage', function(data){
        chatmessage(socket, data);
    });

    // client subscribtion to a room
    socket.on('subscribe', function(data){
        subscribe(socket, data);
    });

    // client subscribtion to a private room
    socket.on('subscribeprivate', function(data){
        subscribeprivate(socket, data);
    });

    // two client subscribtion to a private room
    socket.on('subscribeprivatetwoclient', function(data){
        subscribeprivatetwoclient(socket, data);
    });

    // client subscribtion to a room
    socket.on('getusersonchannel', function(data){
        socket.emit('usersonchannel', { room: data.room, clients: getClientsInRoom(socket.id, data.room) });
    });

    // Get list of active channel
    socket.on('getchannellist', function(data){
        getChannelList(socket, data);
    });

    // client unsubscribtion from a room
    socket.on('unsubscribe', function(data){
        unsubscribe(socket, data);
    });

    // register a room
    socket.on('registerroom', function(data){
        registerRoom(socket, data);
    });

    // ban user
    socket.on('trybanuser', function(data){
        banUser(socket, data);
    });

    // when a client calls the 'socket.close()'
    // function or closes the browser, this event
    // is built in socket.io so we actually dont
    // need to fire it manually
    socket.on('disconnect', function(){
        disconnect(socket);
    });
});

// create a client for the socket
function connect(socket, data){
    //chick if user not ban
    var mongoose = require('mongoose');
    var User = mongoose.model('User');

    //generate clientId
    data.clientId = generateId();
    data.socketId = socket.id;

    User.findOne({ username: data.nickname }, function (err, user) {
        if (err) { return done(err) }

        if (user.isBlocked) {
            delete chatClients[socket.id];
        } else {
            // now the client objtec is ready, update
            // the client
            socket.emit('ready', { clientId: data.clientId });

            // auto subscribe the client to the 'System'
            subscribe(socket, { room: 'System' });

            var rooms = getRooms();
            var validRooms = [];
            for (var i = 0; i < rooms.length; i++) {
                if (rooms[i].indexOf("/private-") != 0) {
                    validRooms.push(rooms[i]);
                }
            }

            socket.emit('roomslist', { rooms:  validRooms});
        }
    })

    // save the client to the hash object for
    // quick access, we can save this data on
    // the socket with 'socket.set(key, value)'
    // but the only way to pull it back will be
    // async
    chatClients[socket.id] = data;
}

function banUser(socket, data) {
    if (data.room.indexOf("private-") != 0 && data.user != data.moderator) {
        var mongoose = require('mongoose');
        var User = mongoose.model('User');

        User.findOne({ username: data.moderator }, function (err, moderator) {
            if (err) { return done(err) }

            if (moderator.rooms.indexOf(data.room) > -1 || moderator._id == 0) {
                // ban

                User.findOne({ username: data.user }, function (err, bannedUser) {
                    if (err) { return done(err) }

                    bannedUser.isBlocked = true;
                    bannedUser.save();
                })

                socket.broadcast.to(data.room).emit('chatmessage', { client: chatClients[socket.id], message: "User " + data.user + " was banned by " + data.moderator + ".", room: data.room });

                socket.broadcast.to(data.room).emit('banuser', { user: data.user, moderator: data.moderator, room: data.room, message: "User " + data.user + " was banned by " + data.moderator + "."});

                for (var key in chatClients) {
                    if (chatClients[key].nickname == data.user) {
                        disconnect(io.sockets.socket(chatClients[key].socketId));
                    }
                }
            } else {
                // moderator have not permission for baning this user
                socket.emit('notbanuser', { user: data.user, moderator: data.moderator, room: data.room, message : "You have not permission for banning user " + data.user });
            }
        })
    }
}

function registerRoom(socket, data) {

    if (data.room != systemRoom && data.room.indexOf("private-") != 0) {
        var mongoose = require('mongoose');
        var User = mongoose.model('User');

        User.findOne({ username: data.user }, function (err, user) {
            if (err) { return done(err) }

            var canInsertRoom = true;

            User.find({}, function (err, users) {
                for (var j = 0; j < users.length; j++) {
                    var user = users[j];
                    for (var i = 0; i < user.rooms.length; i++) {
                        if (user.rooms[i] == data.room) {
                            canInsertRoom = false;
                            break;
                        }
                    }
                }

                if (canInsertRoom) {
                    user.rooms.push(data.room);
                    user.save();
                    socket.emit('respondregisterroom', { room: data.room });
                } else {
                    socket.emit('respondregisterroom', { room: data.room, hasError: true, error: "Room " + data.room + " has already existed!" });
                }
            });
        })
    } else {
        socket.emit('respondregisterroom', { room: data.room, hasError: true, error: "You do not have permission to register this room!" });
    }
}

// when a client disconnect, unsubscribe him from
// the rooms he subscribed to
function disconnect(socket){
    // get a list of rooms for the client
    var rooms = io.sockets.manager.roomClients[socket.id];

    // unsubscribe from the rooms
    for(var room in rooms){
        if(room && rooms[room]){
            unsubscribe(socket, { room: room.replace('/','') });
        }
    }

    // client was unsubscribed from the rooms,
    // now we can selete him from the hash object
    delete chatClients[socket.id];
}

// receive chat message from a client and
// send it to the relevant room
function chatmessage(socket, data){
    // by using 'socket.broadcast' we can send/emit
    // a message/event to all other clients except
    // the sender himself
    socket.broadcast.to(data.room).emit('chatmessage', { client: chatClients[socket.id], message: data.message, room: data.room });
}

function subscribeprivate(socket, data){
    // subscribe the client to the room
    socket.join(data.room);

    // update all other clients about the online
    // presence
    updatePresence(data.room, socket, 'online');

    // send to the client a list of all subscribed clients
    // in this room
    socket.emit('roomclients', { room: data.room, clients: getClientsInRoom(socket.id, data.room) });

    var secondClientId = null;

    for (var key in chatClients) {
        if (chatClients[key].nickname == data.secondNick) {
            secondClientId =  chatClients[key].socketId;
        }
    }

    if (secondClientId != null) {
        socket.broadcast.emit('privateroomopen', { clientId : secondClientId, firstNick : data.currentNick });
    }
}

function subscribeprivatetwoclient(socket, data){
    // subscribe the client to the room
    socket.join(data.room);

    // update all other clients about the online
    // presence
    updatePresence(data.room, socket, 'online');

    // send to the client a list of all subscribed clients
    // in this room
    socket.emit('roomclients', { room: data.room, clients: getClientsInRoom(socket.id, data.room) });
}

// subscribe a client to a room
function subscribe(socket, data){
    if (data.room.indexOf("/private-") == 0) return;

    // get a list of all active rooms
    var rooms = getRooms();

    // check if this room is exist, if not, update all
    // other clients about this new room
    if(rooms.indexOf('/' + data.room) < 0){
        socket.broadcast.emit('addroom', { room: data.room });
    }

    // subscribe the client to the room
    socket.join(data.room);

    // update all other clients about the online
    // presence
    updatePresence(data.room, socket, 'online');

    // send to the client a list of all subscribed clients
    // in this room
    socket.emit('roomclients', { room: data.room, clients: getClientsInRoom(socket.id, data.room) });
}

//Get list of channels
function getChannelList(socket, data) {
    var rooms = getRooms();
    var validRooms = [];
    for (var i = 0; i < rooms.length; i++) {
        if (rooms[i].indexOf("/private-") != 0) {
            validRooms.push(rooms[i]);
        }
    }

    socket.emit('roomslist', { rooms:  validRooms});
}

// unsubscribe a client from a room, this can be
// occured when a client disconnected from the server
// or he subscribed to another room
function unsubscribe(socket, data){
    // update all other clients about the offline
    // presence
    updatePresence(data.room, socket, 'offline');

    // remove the client from socket.io room
    socket.leave(data.room);

    // if this client was the only one in that room
    // we are updating all clients about that the
    // room is destroyed
    if(!countClientsInRoom(data.room)){

        // with 'io.sockets' we can contact all the
        // clients that connected to the server
        io.sockets.emit('removeroom', { room: data.room });
    }
}

// 'io.sockets.manager.rooms' is an object that holds
// the active room names as a key, returning array of
// room names
function getRooms(){
    return Object.keys(io.sockets.manager.rooms);
}

// get array of clients in a room
function getClientsInRoom(socketId, room){
    // get array of socket ids in this room
    var socketIds = io.sockets.manager.rooms['/' + room];
    var clients = [];

    if(socketIds && socketIds.length > 0){
        socketsCount = socketIds.lenght;

        // push every client to the result array
        for(var i = 0, len = socketIds.length; i < len; i++){

            // check if the socket is not the requesting
            // socket
            if(socketIds[i] != socketId){
                clients.push(chatClients[socketIds[i]]);
            }
        }
    }

    return clients;
}

// get the amount of clients in aroom
function countClientsInRoom(room){
    // 'io.sockets.manager.rooms' is an object that holds
    // the active room names as a key and an array of
    // all subscribed client socket ids
    if(io.sockets.manager.rooms['/' + room]){
        return io.sockets.manager.rooms['/' + room].length;
    }
    return 0;
}

// updating all other clients when a client goes
// online or offline.
function updatePresence(room, socket, state){
    // socket.io may add a trailing '/' to the
    // room name so we are clearing it
    room = room.replace('/','');

    // by using 'socket.broadcast' we can send/emit
    // a message/event to all other clients except
    // the sender himself
    socket.broadcast.to(room).emit('presence', { client: chatClients[socket.id], state: state, room: room });
}

// unique id generator
function generateId(){
    var S4 = function () {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
}

// show a message in console
console.log('Chat server is running and listening to port %d...', port);