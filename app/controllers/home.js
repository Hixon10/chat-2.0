
/**
 * Module dependencies.
 */

var mongoose = require('mongoose')
  , User = mongoose.model('User')

/**
 * Index
 */

exports.index = function(req, res){
    if (req.isAuthenticated()) {
        return res.redirect('/chat')
    }

    res.render('home/index', {
        title: 'Home Page',
        user: new User(),
        message: req.flash('error')
    })
}


